#
# Create virtual hosts here by duplicating the example vhost below.
# Replace words like "project" and "project name" and the path to your project's folder according to your needs (~8 occurence)
# 
# Don't forget to enable your virtual hosts (a2ensite) at the end of the file
#

# Project Name
echo '<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName project.dev
        DocumentRoot "/var/www/path/to/project"

        <Directory "/var/www/path/to/project">
                DirectoryIndex index.php
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                Allow from all
                Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/project_error.log

        LogLevel warn
        CustomLog ${APACHE_LOG_DIR}/project_access.log combined
</VirtualHost>
' > /etc/apache2/sites-available/project.conf

# Sample site with SSL enabled
# Note the port nukber and the 3 lines of extra code
# This has to be enabled, too
# Project Name
echo '<VirtualHost *:443>
        ServerAdmin webmaster@localhost
        ServerName project.dev

        SSLEngine on
        SSLCertificateFile    /etc/apache2/ssl/apache.crt
        SSLCertificateKeyFile /etc/apache2/ssl/apache.key

        DocumentRoot "/var/www/path/to/project"

        <Directory "/var/www/path/to/project">
                DirectoryIndex index.php
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                Allow from all
                Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/project_ssl_error.log

        LogLevel warn
        CustomLog ${APACHE_LOG_DIR}/project_ssl_access.log combined
</VirtualHost>
' > /etc/apache2/sites-available/project-ssl.conf

# enabling vhosts
cd /etc/apache2/sites-available/ # don't duplicate or modify this line
a2ensite project.conf
a2ensite project-ssl.conf

# reload the service to apply newly added config files
service apache2 reload