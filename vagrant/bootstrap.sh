#!/bin/bash

# Clearing previous Apache2 sites-available folder
rm -rf /etc/apache2/sites-available/*
rm -rf /etc/apache2/sites-enabled/*

export DEBIAN_FRONTEND=noninteractive

# Creating SWAP file
sudo fallocate -l 1G /var/swapfile > /dev/null 2>&1
sudo chmod 600 /var/swapfile > /dev/null 2>&1
sudo mkswap /var/swapfile > /dev/null 2>&1
sudo swapon /var/swapfile > /dev/null 2>&1

# Adding Postgres Repo to list
echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > /etc/apt/sources.list.d/pgdg.list

# User and Password for the database
DBUSER=root
DBPASSWD=root
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

# Installing myadmin
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none" | debconf-set-selections

apt-get update -y > /dev/null 2>&1
apt-get install wget vim curl apache2 mysql-server php5 php5-cli php5-curl php5-imap php5-common php5-pgsql php5-mcrypt php5-gd php5-imagick php5-sqlite php5-xmlrpc git postgresql-9.4 phpmyadmin -y --force-yes  > /dev/null 2>&1

a2enmod rewrite > /dev/null 2>&1

php5enmod imap

# Install MySQL
mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root'; FLUSH PRIVILEGES;" > /dev/null 2>&1
sed -i 's/bind-address/# bind-address/g' /etc/mysql/mysql.conf.d/mysqld.cnf
service mysql restart

# Set correct timezone in php.ini for Client and Apache versions 
sed -i 's#;date.timezone =#date.timezone = Europe/Budapest#g' /etc/php5/apache2/php.ini
sed -i 's#;date.timezone =#date.timezone = Europe/Budapest#g' /etc/php5/cli/php.ini

# Install and wait for Postgres to start
service postgresql start
# echo "starting postgres"
# while ! service postgresql status | grep "online"; do sleep 1; done;

# Config Postgres
su postgres -c 'createuser -s vagrant' > /dev/null 2>&1
su postgres -c 'createdb vagrant' > /dev/null 2>&1

HBAFILE="$(psql -U postgres -t -P format=unaligned -c 'show hba_file')"
echo "local   all             postgres                                trust
local   all             all                                     trust
host    all             all             0.0.0.0/0               trust
host    all             all             127.0.0.1/32            trust
host    all             all             ::1/128                 trust
" > "$HBAFILE"
PGCONFIGFILE="$(psql -U postgres -t -P format=unaligned -c 'show config_file')"
echo "listen_addresses = '*'" >> "$PGCONFIGFILE"
service postgresql restart

# Install composer
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer > /dev/null 2>&1

# Adding Default virtualhost
echo "<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www

        <Directory />
                Options FollowSymLinks
                AllowOverride All
        </Directory>
        <Directory /var/www/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

        ErrorLog \${APACHE_LOG_DIR}/error.log

        LogLevel warn
        CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
" > /etc/apache2/sites-available/000-default.conf

echo '<IfModule mod_ssl.c>
    <VirtualHost *:443>
            ServerAdmin webmaster@localhost
            DocumentRoot /var/www/html
            ErrorLog ${APACHE_LOG_DIR}/error.log
            CustomLog ${APACHE_LOG_DIR}/access.log combined
            SSLEngine on
            SSLCertificateFile      /etc/apache2/ssl/apache.crt
            SSLCertificateKeyFile /etc/apache2/ssl/apache.key
            <FilesMatch "\.(cgi|shtml|phtml|php)$">
                            SSLOptions +StdEnvVars
            </FilesMatch>
            <Directory /usr/lib/cgi-bin>
                            SSLOptions +StdEnvVars
            </Directory>
            BrowserMatch "MSIE [2-6]" \
                            nokeepalive ssl-unclean-shutdown \
                            downgrade-1.0 force-response-1.0
            BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
    </VirtualHost>  
</IfModule>' > /etc/apache2/sites-available/default-ssl.conf

# SSL
a2enmod ssl > /dev/null 2>&1
mkdir /etc/apache2/ssl > /dev/null 2>&1
openssl req -new -newkey rsa:4096 -days 36500 -nodes -x509 -subj "/C=HU/ST=Budapest/L=Budapest/O=Organization/CN=www.example.com" -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.crt > /dev/null 2>&1

service apache2 restart