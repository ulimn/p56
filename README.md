# Martin's Vagrant for PHP 5.6

## Info:
Vagrant VM for PHP 5.6 with Apache, MySQL, PostgreSQL, Git, Composer, Symfony and FuelPHP installers.

Base is Ubuntu 15.04.
## Installation:

1. Install VirtualBox (https://www.virtualbox.org/)
2. Install Vagrant (https://www.vagrantup.com/)
3. Optional: Install Git for Windows for Git Bash
4. "git clone https://ulimn@bitbucket.org/ulimn/p56.git p56"
5. Create a copy of _sample.synced_folders.yml_ and _vagrant/sample.vhosts.sh_
6. Edit above files according to your needs
7. "cd p56"
8. "vagrant up"

## Troubleshooting:
- **Q:** Cmd.exe or Git Bash error message like **tty** is not terminal
- **A:** Run once: echo "export VAGRANT_DETECTED_OS=cygwin" >> ~/.bashrc

## About Vagrant:
Read more at https://docs.vagrantup.com.